print ("A program to print out the vowels in a string by Hamplus ---\
+23463047037")
word ="Favour"
word = word.lower ()
vowels = ['a','e','i','o','u']
for i in range(1,len(word)):
	if (vowels[i-1] in word):
		print (vowels[i-1])

print()
print ("A program to print out the consonants in a string by Hamplus ---\
+23463047037")
word ="Favour"
word = word.lower ()
vowels = list('aeiou')
for i in range(1,len(word)):
	if (word[i-1] not in vowels):
		print (word[i-1])

print()
print ("A program to check if a string contains vowel by Scofield")
print()
word = str(input("Enter any sentence or word and I'll\
check if i contains 'VOWELS': "))
words = word.lower()
vowel = "aeiou"
y = any (x in words for x in vowel)
if y == True:
    print ("Yeah; " + word + " contains 'vowels'")
else:
    print ("Nah; Wrong " + word + " does not contain 'vowels'")

print()
print ("A program to find the smallest divisor other than 1 of a number\
 by Hamplus --- +23463047037")
while True:
        number = input("To terminate this program type (end)\n\
Please enter a number\n")
        check = number.isnumeric()
        if number == 'end':
                break
        else:
                try:
                        number = int(number)
                        divisors = list()
                        for numbers in range(2,number+1):
                                if number%numbers == 0:
                                        divisors.append(numbers)
                        smallest_divisor = min(divisors)
                        print ("Divisors for", number, ":",divisors)
                        print ("Smallest Divisor > 1 is: ", smallest_divisor)
                except:
                        print ("Enter a numeral not a string")
print ("Thanks for your time")

print()
print ("A program to grade different scores in an output\
 by Hamplus --- +23463047037")
scores = list();grades = list()
while True:
        score = input("To terminate, type 'stop'.\nEnter \
a score to grade please\n")
        if score == 'stop':
                break
        else:
                try:
                        gradeList = ["A","B","C","D","F"]
                        #90-100 = A; 80-89 = B; 70-79 = C; 60-69 = D; 0-59 = F
                        score = int(score)
                        if score < 0 or score > 100:
                                print ("Your score must be between 0 and 100")
                        elif score >= 90 and score <= 100:
                                grade = gradeList[0]
                                scores.append(score)
                                grades.append(grade)
                        elif score>= 80 and score <= 89:
                                grade = gradeList[1]
                                scores.append(score)
                                grades.append(grade)
                        elif score>= 70 and score <= 79:
                                grade = gradeList[2]
                                scores.append(score)
                                grades.append(grade)
                        elif score >= 60 and score <= 69:
                                grade = gradeList[3]
                                scores.append(score)
                                grades.append(grade)
                        else:
                                grade = gradeList[4]
                                scores.append(score)
                                grades.append(grade)
                except:
                        print ("Please a numeral is needed not a string.")
for i in range(1,len(grades)):
        print (scores[i-1],"\t",grades[i-1])
print ("Thank you for using this system.")
