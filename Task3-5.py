print("Solution to Exercise 5")
print("-----------------------")
print()
original = """Twinkle,twinkle,little star , How I wonder what you are!
Up above the world so high, like a diamond in the sky"""
scrambled = """eltwink twinkle ttleli rast woh reonwd i tahw uoy rea! pu eovba
world os hgih ekil a donmaid in the ysk"""
print ("Original Sentence:\n", original)
print()
print ("Scrambled Sentence:\n", scrambled)
scrambled_words=scrambled.split()
word0 = scrambled_words[0]
word1 = scrambled_words[1]
word2 = scrambled_words[2]
word3 = scrambled_words[3]
word4 = scrambled_words[4]
word5 = scrambled_words[5]
word6 = scrambled_words[6]
word7 = scrambled_words[7]
word8 = scrambled_words[8]
word9 = scrambled_words[9]
word10 = scrambled_words[10]
word11 = scrambled_words[11]
word12 = scrambled_words[12]
word13 = scrambled_words[13]
word14 = scrambled_words[14]
word15 = scrambled_words[15]
word16 = scrambled_words[16]
word17 = scrambled_words[17]
word18 = scrambled_words[18]
word19 = scrambled_words[19]
word20 = scrambled_words[20]
word_0 = word0[2].capitalize()+ word0[3:]+word0[1]+word0[0]
word_1 = word1
word_2 = word2[4:] + word2[:4]
word_3 = word3[2:] + word3[1]+word3[0]
word_4 = word4[2].capitalize() + word4[1] + word4[0]
word_5 = word5[4] + word5[2:4] + word5[5] + word5[1]+word5[0]
word_6 = word6.capitalize()
word_7 = word7[3] + word7[2] + word7[1] + word7[0]
word_8 = word8[2] + word8[1] + word8[0]
word_9 = word9[2] + word9[0] + word9[1] + word9[3]
word_10 = word10[1].capitalize() + word10[0]
word_11 = word11[4] + word11[3] + word11[1:3] + word11[0]
word_12 = word12
word_13 = word13[1] + word13[0]
word_14 = word14[0] + word14[2] + word14[1] + word14[3]
word_15 = word15[3] + word15[2] + word15[1] + word15[0]
word_16 = word16
word_17 = word17[6] + word17[5] + word17[4] + word17[3] + word17[1:3] + word17[0]
word_18 = word18
word_19 = word19
word_20 = word20[1:] + word20[0]
solution = (word_0 + ", " + word_1 + ", " + word_2 + " " + word_3 +
", " + word_4 + " " + word_6 + " " + word_5 + " " + word_7 + " " + word_8
    + " " + word_9 + "\n " + word_10 + " " + word_11 + " " + word_12 + " " +
word_13 + " " + word_14 + ", " + word_15 + " " + word_16 + " " + word_17 + " "
+ word_18 + " " + word_19 + " " + word_20)
print ("The rearranged word is:\n", solution)
