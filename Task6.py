print ("Assignment Done By Hampo, JohnPaul - - - 07063047037")
print("Solution to Assignment 1")
print("-----------------------")
print()
print ("This is a pay computation program")
rate = input("Enter the rate per hour\n")
rate = int(rate)
hours = input("Please enter the hours worked for\n")
a = hours.isnumeric()
while True:
    if (a == True):
        hours = int(hours)
        if (hours > 50) and (hours < 70):
            pay = 2 * 1.5 * rate * hours
            print ("Your Total Pay is\n", pay)
        elif (hours > 70):
            pay = 3 * 1.5 * rate * hours
            print ("Your Total Pay is\n", pay)
        elif (hours > 40):
            pay = 1.5 * rate * hours
            print ("Your Total Pay is\n", pay)
        else:
            pay = rate * hours
            print ("Your Total Pay is\n", pay)
        break
    else:
        print ("Your hours must be numeric")
        hours = input("Please enter the hours worked for\n")
        continue
print()
print("Solution to Assignment 2")
print("-----------------------")
print()
print ("This program removes the 3rd item from a list till the list is empty")
a =list()
b = int(input("enter the length of list you want\n"))
while (int(b)>len(a)):
	c = (input("enter an item\n"))
	a.append(c)
print(a)
while len(a)>3:
    del a[2]

print (a)
print()
print("Solution to Assignment 3")
print("-----------------------")
print()
print ("This program prints a long text, \
converts it to a list and prints the words in the long text")
long_text = input("Please type in the long text\n")
print ("This is the text you typed\n", long_text)
list_long_text = long_text.split()
print ("This is the list of words from the text you typed\n", list_long_text)
count = 0
# This prints the words and their position. I did this while trying for the solution
for word in list_long_text:
    count = count + 1
    print (word, " ", count)
# The count of apperance is here below!
print()
print("Here is the words and their count of apperance")
for word in list_long_text:
    print (word, " ", list_long_text.count(word))
print()
print("Solution to Assignment 4")
print("-----------------------")
print()
print ("This program finds the number of divisors of an integer\
whether the integer is even or odd")
a = input("Please enter any integer\n")
a = int(a)
even = int(a.__truediv__(2))
odd = int(a.__truediv__(3))
print ("The number of divisors for", a, "evenly is\n", even)
print ("The number of divisors for", a, "oddly is\n", odd)
print()
print("Solution to Assignment 5")
print("-----------------------")
print()
print ("This program checks if a string starts with a specified character")
word = input("Please enter the string\n")
character = input ("Please enter the specified character\n")
word = word.lower (); character = character.lower()
if (word.startswith(character) == True):
    print ("Congrats!", word, "starts with", character, "as specified")
else:
    print ("No! No!! No!!!", word, "didn't start with", character,
    "as specified.", word, "starts with", word[0])
    
