print ("Assignment 3 - Task 7 @ WittyTech Bootcamp by Hamplus Tech")
print ()
print ("Question 1 Solution")
print("------------------------")
print()
score = input("Please enter a score between 0.0 and 1.0\n")
input_char = score.isalpha ()
if input_char == True:
    print ("Bad Score")
else:
    score = float(score)
    if (score >1.0):
        print("Bad Score")
    elif (score>=0.9):
        print ("A")
    elif (score>=0.8):
        print("B")
    elif (score>=0.7):
        print ("C")
    elif (score>=0.6):
        print ("D")
    elif (score<0.6):
        print("F")

print ()
print ("Question 2 Solution")
print("------------------------")
print()
number = input("Please enter a number to determine whether it is EVEN or ODD\n")
check = number.isalpha()
if (check == True):
    print ("Please enter a number not an alphabelt")
else:
    number = float(number)
    if (number%2 == 0):
        print ("The number",number,"is an even number")
    else:
        print ("The number", number, "is an odd number")

    # An alternative way
    print ("This was solved using an alternative way")
    check = number.__divmod__(2)[1]
    if (check == 0):
        print ("The number",number,"is an even number")
    else:
        print ("The number", number, "is an odd number")
print ()
print ("Question 3 Solution")
print("------------------------")
print()
word = input("Please enter any string\n")
check = word.isalpha()
if check == True:
    word_len = len(word)
    if word_len >= 3:
        if word.endswith("ing"):
            word_out = word + "ly"
            print("A change has occured! This is the input:", word, ";\
and this is the output:",word_out)
        else:
            word_out = word + "ing"
            print("A change has occured! This is the input:", word, ";\
and this is the output:",word_out)
    else:
        word_out = word
        print ("Nothing was changed! This is the input:", word, ";\
and this is the output:",word_out)
else:
    print("Please enter a word(s) not number")
print ()
print ("Question 4 Solution")
print("------------------------")
print()
data_list = [1452, 11.23, 1+2j, True,
             'google', (0, -1), [5,12], {"class":'V',"section":'A'}]
for data in data_list:
    print ("Data is: ", data, "\tData Type is :", type(data))
print ("Thank you!")
print ()
print ("Question 5 Solution")
print("------------------------")
print()
def square():
    'Creates and Prints a square of numbers from 1 to 30'
    square_numbers = [numbers**2 for numbers in range(1,30)]
    return square_numbers

print ()
print ("Question 6 Solution")
print("------------------------")
print()

def pay_compute(hour,rate):
    'Computes pay with extra time benefit'
    hour=float(hour); rate=float(rate)
    def_hour = 40
    if hour>def_hour:
        extra_hour = hour - def_hour
        extra_rate = rate + (rate/2)
        pay = (def_hour * rate) + (extra_hour * extra_rate)
    else:
        pay = hour * rate
    print ('Pay: ',pay)




##a = [1,2,3,4,5,6,7,8,9,10,11,12]
##b = [1,2,3,4,5,6,7,8,9,10,11,12]
##for i in a:
##	for j in b:
##		prod = a[i-1] * b[j-1]
##		print (prod)
