# Gets the utme score from the candidate
score = input("Please enter your score\n")
#checks if entered score is numeric
check = score.isnumeric()
federal_universities = ["FUPRE","UNN","UNIZIK","FUNIA","UNIBEN"]
state_universities = ["DELSU","RSUT","IMSU", "EBSU", "LASU"]
poly_mono = ["Ozoro", "Oghara", "Abia Poly", "Anambra Poly", "Ikeja Poly"]
college = ["Agbor", "Asaba","Warri","Onitsha","Iwollo"]
while True:
    if check == True:
    #converts the entered to integer
        score = int(score)
    #does the comparison and recommendation
        if score>400:
            print ("Error! You can't score more than 400 in UTME")
        elif score>=250:
            print ("You are qualified for admission into \
any federal university, such as:")
            for school in federal_universities:
                print (school)
        elif score>=200 and score<250:
            print ("You are qualified for admission \
into any state university, such as:")
            for school in state_universities:
                print (school)
        elif score>=180 and score<200:
            print ("You are qualified for admission \
into any polytechnic/monotechnic, such as:")
            for school in poly_mono:
                print (school)
        elif score>=150 and score<180:
            print ("You are qualified for admission \
into any college of education, such as:")
            for school in college:
                print (school)
        else:
            print("Sorry! You have to retake UTME exams next session.")
        break
    else:
        print ("Enter a number not a string")
        score = input("Please enter your score\n")
        continue
#the system quits
print ("Thank you for using our system.")
